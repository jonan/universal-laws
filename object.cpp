/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

#include "object.hpp"

#include "chipmunk/chipmunk.h"

#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>

#include "graphics.hpp"
#include "level.hpp"

// Constructor
Object::Object(void) : body(NULL), shape(NULL), level(NULL) {
  screen = video::Graphics::getInstance();
  int num = 4;
  cpVect verts[] = {
    cpv(-25,-25),
    cpv(-25, 25),
    cpv( 25, 25),
    cpv( 25,-25),
  };
  // Create a box and initialize some of its parameters.
  body = cpBodyNew(1.0, cpMomentForPoly(1.0, num, verts, cpvzero));
  shape = cpPolyShapeNew(body, num, verts, cpvzero);
  shape->e = 0.0;
  shape->u = 1.5;
}

// Destructor
Object::~Object(void) {
  if (level) level->removeObject(*this);
  if (body ) cpBodyFree(body);
  if (shape) cpShapeFree(shape);
}

// 
void Object::setLevel(Level *level) {
  if (this->level) this->level->removeObject(*this);
  this->level = level;
  if (this->level) this->level->addObject(*this);
}

// 
void Object::setPosition(const float x, const float y) {
  body->p = cpv(x,y);
}

// 
void Object::draw(void) {
  SDL_Rect pos;
  cpBB bounding_box = cpShapeCacheBB(shape);
  pos.x = bounding_box.l;
  pos.y = bounding_box.b;
  screen->draw(screen->getImage("box", video::OPAQUE, video::NONE, -(body->a*180.0)/3.14159265), pos);
}
