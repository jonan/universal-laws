/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

#include "events.hpp"

#include <SDL/SDL.h>

#include "events_listener.hpp"

// Singleton pattern constructor
Events* Events::getInstance(void) {
  static Events instance;
  return &instance;
}

// Destructor
Events::~Events(void) {
  delete event;
}

// 
void Events::readInput(void) {
  std::list<EventsListener*>::iterator it = listeners.begin();
  std::list<EventsListener*>::iterator it_end = listeners.end();

  while (SDL_PollEvent(event)) {
    switch (event->type) {
      case SDL_KEYDOWN:
        while(it != it_end) {
          (*it)->keyPressed(event->key.keysym.sym);
          it++;
        }
        break;
      case SDL_KEYUP:
        while(it != it_end) {
          (*it)->keyReleased(event->key.keysym.sym);
          it++;
        }
        break;
      default:
        // Impossible case
        break;
    }
  }
}

// Constructor
Events::Events(void) {
  event = new SDL_Event;
  // Ignore all unused events
  SDL_EventState(SDL_ALLEVENTS, SDL_IGNORE);
  SDL_EventState(SDL_KEYDOWN, SDL_ENABLE);
  SDL_EventState(SDL_KEYUP, SDL_ENABLE);
  // Hide default cursor
  SDL_ShowCursor(SDL_DISABLE);
}
