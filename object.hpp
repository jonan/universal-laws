/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

/// @file
/// 
/// @author Jonan

#ifndef OBJECT_HPP
#define OBJECT_HPP

#include "macros.hpp"

struct cpBody;
struct cpShape;

namespace video {
class Graphics;
}

class Level;

/// 
class Object {
  public:
    Object(void); // Constructor
    ~Object(void); // Destructor

    // @{
    /// 
    cpBody*  getBody  (void) const {return body; }
    cpShape* getShape (void) const {return shape;}
    // @}

    // @{
    /// 
    void setLevel    (Level *level);
    void setPosition (const float x, const float y);
    // @}

    /// 
    void draw(void);

  private:
    video::Graphics *screen;

    cpBody *body;
    cpShape *shape;

    Level *level;

    DISALLOW_COPY_AND_ASSIGN(Object);
};

#endif // OBJECT_HPP
