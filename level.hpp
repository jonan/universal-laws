/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

/// @file
/// 
/// @author Jonan

#ifndef LEVEL_HPP
#define LEVEL_HPP

#include <list>

#include "events_listener.hpp"
#include "loop.hpp"

// chipmunk
struct cpBody;
struct cpSpace;

class Object;

/// 
class Level : public EventsListener, public GameLoop {
  public:
    Level(void); // Constructor
    virtual ~Level(void); // Destructor

    // @{
    // 
    void addObject    (Object &object);
    void removeObject (Object &object);
    // @}

    /// 
    void start(void);

  private:
    // 
    virtual void keyReleased(const int key);

    // 
    void draw(void);

    // 
    virtual bool frame(Uint32 elapsed_time);

    bool end_level;

    cpSpace *space;
    cpBody *ground;

    std::list<Object*> objects;

    DISALLOW_COPY_AND_ASSIGN(Level);
};

#endif // LEVEL_HPP
