/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

#include "level.hpp"

#include <algorithm>

#include "chipmunk/chipmunk.h"

#include "events.hpp"
#include "graphics.hpp"
#include "object.hpp"

// Constructor
Level::Level(void) : end_level(false) {
  cpResetShapeIdCounter();
  space = cpSpaceNew();
  cpSpaceResizeStaticHash(space, 20.0, 999);
  space->gravity = cpv(0, 100);

  ground = cpBodyNew(INFINITY, INFINITY);

  // Set floor
  cpShape *shape;
  shape = cpSegmentShapeNew(ground, cpv(0,786), cpv(1024,786), 0.0f);
  shape->e = 1.0;
  shape->u = 1.0;
  cpSpaceAddStaticShape(space, shape);
  // Add boxes
  Object *obj = new Object;
  obj->setPosition(300.0, 420.0);
  addObject(*obj);
  obj = new Object;
  obj->setPosition(305.0, 500.0);
  addObject(*obj);
  obj = new Object;
  obj->setPosition(290.0, 200.0);
  addObject(*obj);
  obj = new Object;
  obj->setPosition(345.0, 210.0);
  addObject(*obj);
  obj = new Object;
  obj->setPosition(305.0, 360.0);
  addObject(*obj);
}

// Destructor
Level::~Level(void) {
  std::list<Object*>::iterator it;
  // Every time an object is deleted the lists's size
  // changes, so the iterator must be set to objects.begin()
  for (it = objects.begin(); it != objects.end(); it = objects.begin())
    delete (*it);
}

// 
void Level::addObject(Object &object) {
  if ( find(objects.begin(),objects.end(),&object) == objects.end() ) {
    objects.push_back(&object);
    cpSpaceAddBody(space, object.getBody());
    cpSpaceAddShape(space, object.getShape());
    object.setLevel(this);
  }
}

// 
void Level::removeObject(Object &object) {
  if ( find(objects.begin(),objects.end(),&object) != objects.end() ) {
    objects.remove(&object);
    cpSpaceRemoveBody(space, object.getBody());
    cpSpaceRemoveShape(space, object.getShape());
    object.setLevel(NULL);
  }
}

// 
void Level::start(void) {
  Events::getInstance()->addListener(this);
  loop();
}

// 
void Level::keyReleased(const int key) {
  if (key == SDLK_ESCAPE) end_level = true;
}

// 
void Level::draw(void) {
  std::list<Object*>::iterator it = objects.begin();
  std::list<Object*>::iterator it_end = objects.end();
  video::Graphics::getInstance()->erase();
  for (; it != it_end; it++)
    (*it)->draw();
}

// 
bool Level::frame(Uint32 elapsed_time) {
  draw();
  cpSpaceStep(space, static_cast<float>(elapsed_time)/1000.0);
  return end_level;
}
