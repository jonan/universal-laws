/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

/// @file
/// 
/// @author Jonan

#ifndef EVENTS_HPP
#define EVENTS_HPP

#include <list>

#include "macros.hpp"

union SDL_Event;

class EventsListener;

/// 
class Events {
  public:
    static Events* getInstance(void); // Singleton pattern constructor
    ~Events(void); // Destructor

    /// 
    void readInput(void);

    // @{
    /// 
    void addListener    (EventsListener *listener) {listeners.push_back(listener);}
    void removeListener (EventsListener *listener) {listeners.remove(listener);   }
    // @}

  private:
    Events(void); // Constructor

    SDL_Event *event;
    std::list<EventsListener*> listeners;

    DISALLOW_COPY_AND_ASSIGN(Events);
};

#endif // EVENTS_HPP
