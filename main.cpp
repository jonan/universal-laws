/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

/// @file
/// The main function.
/// @author Jonan

#include "chipmunk/chipmunk.h"

#include "graphics.hpp"
#include "level.hpp"

int main(int argc, char *argv[]) {
  cpInitChipmunk();
  video::Graphics::getInstance()->createWindow(false, 1024, 786);
  Level *level = new Level;
  level->start();
  delete level;
  return 0;
}
