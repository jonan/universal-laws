/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

#include "loop.hpp"

#include "events.hpp"
#include "graphics.hpp"
#include "timer.hpp"

// Constructor
GameLoop::GameLoop(void) : elapsed_time(0) {
  fps = new Timer;
  screen = video::Graphics::getInstance();
  events = Events::getInstance();
}

// Destructor
GameLoop::~GameLoop(void) {
  delete fps;
}

// Starts the loop
void GameLoop::loop(void) {
  done = false;

  while (!done) {
    fps->start();
    events->readInput();
    done = frame(elapsed_time);
    screen->update();
    elapsed_time = fps->end(30);
  }
}
