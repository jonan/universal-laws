# This file is part of Universal Laws.
# Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>
#
# Universal Laws is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Universal Laws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Universal Laws. If not, see <http://www.gnu.org/licenses/>

# Global variables
OBJECTS = events.o \
          graphics.o \
          image.o \
          level.o \
          loop.o \
          main.o \
          object.o \
          timer.o \
          ttf.o
SDL_LIBS = -lSDL -lSDL_ttf -lSDL_image -lSDL_gfx
CFLAGS = -c -g -Wall
CC = g++
EXE = universal-laws

# Headers dependencies
EVENTS = events.hpp \
         $(MACROS)

EVENTS_LISTENER = events_listener.hpp

GRAPHICS = graphics.hpp \
           $(MACROS)

IMAGE = image.hpp \
        $(GRAPHICS)

LEVEL = level.hpp \
        $(EVENTS_LISTENER) \
        $(LOOP)

LOOP = loop.hpp \
       $(MACROS)

MACROS = macros.hpp

OBJECT = object.hpp \
         $(MACROS)

TIMER = timer.hpp \
        $(MACROS)

TTF = ttf.hpp \
      $(GRAPHICS)

# Build dependencies
$(EXE) : $(OBJECTS)
	$(CC) -g -Wall $(SDL_LIBS) -o $(EXE) $(OBJECTS) -Lchipmunk -lchipmunk

events.o : events.cpp $(EVENTS) \
           $(EVENTS_LISTENER)
	$(CC) $(CFLAGS) events.cpp

graphics.o : graphics.cpp $(GRAPHICS) \
             $(IMAGE) \
             $(TIMER) \
             $(TTF)
	$(CC) $(CFLAGS) graphics.cpp

image.o : image.cpp $(IMAGE)
	$(CC) $(CFLAGS) image.cpp

level.o : level.cpp $(LEVEL) \
          $(EVENTS) \
          $(GRAPHICS) \
          $(OBJECT)
	$(CC) $(CFLAGS) level.cpp

loop.o : loop.cpp $(LOOP) \
         $(EVENTS) \
         $(GRAPHICS) \
         $(TIMER)
	$(CC) $(CFLAGS) loop.cpp

main.o : main.cpp \
         $(GRAPHICS)
	$(CC) $(CFLAGS) main.cpp

object.o : object.cpp $(OBJECT) \
           $(GRAPHICS) \
           $(LEVEL)
	$(CC) $(CFLAGS) object.cpp

timer.o : timer.cpp $(TIMER)
	$(CC) $(CFLAGS) timer.cpp

ttf.o : ttf.cpp $(TTF)
	$(CC) $(CFLAGS) ttf.cpp

# Make options
.PHONY : all clean

all: clean
	make

clean :
	rm -f $(EXE) *~ $(OBJECTS)

