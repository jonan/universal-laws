/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

/// @file
/// The GameLoop class.
/// @author Jonan

#ifndef LOOP_HPP
#define LOOP_HPP

#include <SDL/SDL.h>

#include "macros.hpp"

namespace video {
class Graphics;
}

class Events;
class Timer;

/// Creates a normal game loop.
class GameLoop {
  protected:
    GameLoop(void); // Constructor
    virtual ~GameLoop(void); // Destructor

    // Starts the loop
    void loop(void);

  private:
    bool done;
    Timer *fps;
    Uint32 elapsed_time;

    video::Graphics *screen;
    Events *events;

    // Funtion that will be called inside the loop
    virtual bool frame(Uint32 elapsed_time) = 0;

    DISALLOW_COPY_AND_ASSIGN(GameLoop);
};

#endif // LOOP_HPP
