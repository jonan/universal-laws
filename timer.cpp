/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

#include "timer.hpp"

// Indicates the ending point.
Uint32 Timer::end(const Uint32 ms) {
  now = SDL_GetTicks();
  Uint32 elapsed_time = now-before;

  if ( elapsed_time < ms ) {
    SDL_Delay( ms - elapsed_time );
    return ms;
  } else {
    return elapsed_time;
  }
}
