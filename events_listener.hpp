/*
This file is part of Universal Laws.
Copyright (C) 2009 Jon Ander Peñalba <jonan88@gmail.com>

Universal Laws is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

Universal Laws is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Universal Laws. If not, see <http://www.gnu.org/licenses/>
*/

/// @file
/// 
/// @author Jonan

#ifndef EVENTS_LISTENER_HPP
#define EVENTS_LISTENER_HPP

#include <SDL/SDL_keysym.h>

/// 
class EventsListener {
  public:
    virtual ~EventsListener(void) {} // Destructor

    // @{
    /// 
    virtual void keyPressed  (const int key) {}
    virtual void keyReleased (const int key) {}
    // @}

  protected:
    EventsListener(void) {} // Constructor
};

#endif // EVENTS_LISTENER_HPP
